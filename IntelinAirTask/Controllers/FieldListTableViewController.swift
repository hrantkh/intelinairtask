//
//  FieldsListTableViewController.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import UIKit

class FieldListTableViewController: UITableViewController {
    
    private let cellReuseIdentifier = "FieldListCell"
    private let mapViewControllSegue = "MapViewControllerSegue"
    private let cellRowHeight: CGFloat = 50
    private lazy var activityIndicator: UIActivityIndicatorView = {
        UIActivityIndicatorView()
    }()
    private lazy var controller: FieldListController = {
        return FieldListController()
    }()
    
    // MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
    
    //MARK: Setup
    private func configUI() {
        addActivityIndicator()
        showActivity(animate: true)
        getfieldsData()
    }
    
    private func getfieldsData() {
        controller.getFieldsData { error in
            DispatchQueue.main.async {
                if error == nil {
                    self.tableView.reloadData()
                } else {
                    self.showAlert(message: error?.localizedDescription ?? "Some Error")
                }
                self.showActivity(animate: false)
            }
        }
    }
    
    // MARK: UIActivityIndicatorView
    private func addActivityIndicator() {
        activityIndicator.center = self.view.center;
        activityIndicator.hidesWhenStopped = true;
        activityIndicator.style =
            UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = .gray
        view.addSubview(activityIndicator);
    }
    
    private func showActivity(animate: Bool) {
        if animate {
            activityIndicator.startAnimating();
        } else {
            activityIndicator.stopAnimating();
        }
    }
    
    // MARK: UIAlertController
    private func showAlert(message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? MapViewController
        vc?.controller.mapData = sender as? FieldModel
    }
}

// MARK: UITableViewDataSource
extension FieldListTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controller.fieldsData?.fields?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! FildListTableViewCell
        let fieldName = controller.fields?[indexPath.row].fieldTileName()
        cell.configureWith(fieldName: fieldName)
        return cell
    }
}

// MARK: UITableViewDelegate
extension FieldListTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: mapViewControllSegue, sender: controller.fieldsData?.fields?[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellRowHeight
    }
    
}
