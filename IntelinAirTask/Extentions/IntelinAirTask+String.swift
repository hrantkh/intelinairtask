//
//  IntelinAirTask+String.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import Foundation

extension String {
    
    func removePrefix(text: String) -> String {
        if self.hasPrefix(text) {
            return String(self.dropFirst(text.count))
        } else {
            return self
        }
    }
    
}
