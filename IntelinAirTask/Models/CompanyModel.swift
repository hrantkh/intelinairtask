//
//  CompanyModel.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import Foundation

struct CompanyModel: Codable {
    var name: String?
}
