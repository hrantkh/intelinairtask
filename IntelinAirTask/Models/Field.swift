//
//  Field.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//


import Foundation
import MapKit

struct Field: Codable {
    var name: String?
    var boundary: String?
    var centerLongitude: Double?
    var centerLatitude: Double?
}

extension Field {
    
    func coordinates() -> [[CLLocationCoordinate2D]]? {
        guard let pathes = self.parseBoundaryToCoordinates() else { return nil }
        var coordinArray = [[CLLocationCoordinate2D]]()
        for path in pathes {
            coordinArray.append(path.map({ coordinate2DFromString(string: $0.removePrefix(text: " ")) ?? CLLocationCoordinate2D(latitude: 0, longitude: 0)}))
        }
        return coordinArray
    }
    
    private func parseBoundaryToCoordinates() -> [[String]]? {
        guard var stringArray = boundary?.replacingOccurrences(of: "MULTIPOLYGON (((", with: "").replacingOccurrences(of: ")))", with: "").replacingOccurrences(of: "))", with: ")").components(separatedBy: ")") else { return nil }
        
        stringArray = stringArray.map({$0.replacingOccurrences(of: "(", with: "")})
        return stringArray.map({$0.components(separatedBy: ",")})
    }
    
    private func coordinate2DFromString(string: String) -> CLLocationCoordinate2D? {
        guard let longitudeString = string.components(separatedBy: " ").first, let longitude = Double(longitudeString),let latitudeString = string.components(separatedBy: " ").last, let latitude = Double(latitudeString) else { return nil }
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
}
