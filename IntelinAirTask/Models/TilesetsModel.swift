//
//  TilesetsModel.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/21/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import Foundation

struct TilesetsModel: Codable {
    var name: String?
    var path: String?
}

extension TilesetsModel {
    
    func tilesetsUrl() -> String {
        return (path ?? "") + "/{z}/{x}/{y}.png"
    }
    
}
