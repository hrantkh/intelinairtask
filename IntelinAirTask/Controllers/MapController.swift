//
//  MapController.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//


import Foundation

class MapController: NSObject {
    
    var tileUrl = ""
    var mapData: FieldModel?
    
    var longitude: Double? {
        get {
            return self.mapData?.field?.centerLongitude
        }
    }
    var latitude: Double? {
        get {
            return self.mapData?.field?.centerLatitude
        }
    }
    var tiles: [TilesetsModel]? {
        get {
            return self.mapData?.latestFlight?.tilesets
        }
    }
}
