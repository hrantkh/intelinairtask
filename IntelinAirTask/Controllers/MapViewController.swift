//
//  MapViewController.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import UIKit
import Mapbox

class MapViewController: UIViewController {
    
    @IBOutlet weak var firstLayerButton: UIButton!
    @IBOutlet weak var secondLayerButton: UIButton!
    @IBOutlet weak var thirdLayerButton: UIButton!
    @IBOutlet weak var mapView: MGLMapView!
    
    private var lastSelectedButton: UIButton?
    private var rasterLayer: MGLRasterStyleLayer?
    private var source: MGLRasterTileSource?
    lazy var controller: MapController = {
        return MapController()
    }()
    
    private  let minimumZoomLevel = 14
    private let maximumZoomLevel = 21
    private let mapZoomLevel = 14.7
    private let tileSize = 256
    
    
    // MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configMap()
        configButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        drawFields()
    }
    
    // MARK: Setup
    func configMap() {
        if let longitude = controller.longitude, let latitude = controller.latitude {
            mapView.setCenter(CLLocationCoordinate2D(latitude: latitude, longitude: longitude), zoomLevel: mapZoomLevel, animated: false)
        }
        mapView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        mapView.tintColor = .darkGray
        mapView.styleURL = MGLStyle.satelliteStreetsStyleURL
    }
    
    func configButtons() {
        var buttonsArray = [firstLayerButton, secondLayerButton, thirdLayerButton]
        for index in 0..<buttonsArray.count {
            buttonsArray[index]?.setTitle(controller.tiles?[index].name, for: .normal)
        }
    }
    
    func updateMap() {
        removeTileLayer()
        let tileUrl = controller.tileUrl
        source = MGLRasterTileSource(identifier: controller.tileUrl, tileURLTemplates: [tileUrl], options: [ .tileCoordinateSystem: MGLTileCoordinateSystem.TMS.rawValue,                                                        .tileSize: tileSize,                                              .minimumZoomLevel: minimumZoomLevel,
                       .maximumZoomLevel: maximumZoomLevel])
        rasterLayer = MGLRasterStyleLayer(identifier: controller.tileUrl, source: source!)
        mapView.style?.addSource(source!)
        mapView.style?.addLayer(rasterLayer!)
    }
    
    func removeTileLayer() {
        if self.rasterLayer != nil {
            mapView.style?.removeLayer(self.rasterLayer!)
        }
        if self.source != nil {
            mapView.style?.removeSource(source!)
        }
    }
    
    // MARK: IBAction
    @IBAction func layerButtonsAction(_ sender: UIButton) {
        if sender == lastSelectedButton {
            sender.backgroundColor = UIColor.layerButtonGreen
            removeTileLayer()
            lastSelectedButton = nil
        } else {
            lastSelectedButton?.backgroundColor = UIColor.layerButtonGreen
            sender.backgroundColor = UIColor.layerButtonRed
            controller.tileUrl = controller.mapData?.latestFlight?.tilesets?[sender.tag].tilesetsUrl() ?? ""
            lastSelectedButton = sender
            updateMap()
        }
    }
}

extension MapViewController: MGLMapViewDelegate {
    
    func mapView(_ mapView: MGLMapView, alphaForShapeAnnotation annotation: MGLShape) -> CGFloat {
        return 0.5
    }
    
    func mapView(_ mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        return .white
    }
    
    func mapView(_ mapView: MGLMapView, fillColorForPolygonAnnotation annotation: MGLPolygon) -> UIColor {
        return UIColor.polygonColor
    }
    
    func drawFields() {
        if let coordinateArray = controller.mapData?.field?.coordinates() {
            for coordinates in coordinateArray {
                let shape = MGLPolygon(coordinates: coordinates, count: UInt(coordinates.count))
                    mapView.addAnnotation(shape)
            }
        }
    }
    
}
