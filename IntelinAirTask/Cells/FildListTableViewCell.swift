//
//  FildListTableViewCell.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import UIKit

class FildListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var fieldsNameLabel: UILabel!

    func configureWith(fieldName: String?) {
        fieldsNameLabel.text = fieldName
    }
    
}
