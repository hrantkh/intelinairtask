//
//  APIClient.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import Foundation

class APIClient {
    
    static let shared = APIClient()
    private  let urlString = "https://api.myjson.com/bins/17lfi0"
    
    func getLocationData(completion: @escaping (FieldsDataModel?, Error?) -> Void) {
        
        guard let url = URL(string: urlString) else { return }
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .useProtocolCachePolicy
        let session = URLSession.init(configuration: config)
        session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                completion(nil, error)
                return
            }
            do {
                let fieldsDataModel = try JSONDecoder().decode(FieldsDataModel.self, from: data!)
                completion(fieldsDataModel, nil)
            } catch {
                completion(nil, error)
            }
            }.resume()
    }
    
}
