//
//  FieldsListController.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import Foundation

class FieldListController: NSObject {
    
    var fieldsData: FieldsDataModel?
    var fields: [FieldModel]? {
        get {
            return fieldsData?.fields
        }
    }
    
    func getFieldsData(completion: @escaping (Error?) -> Void) {
        APIClient.shared.getLocationData { (fieldsDataModel, error) in
            self.fieldsData = fieldsDataModel
            completion(error)
        }
    }
    
}
