//
//  IntelinAirTask+Color.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    @objc static var layerButtonRed: UIColor {
        return #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
    }
    
    @objc static var layerButtonGreen: UIColor {
        return #colorLiteral(red: 0.6117647059, green: 0.7803921569, blue: 0.2784313725, alpha: 1)
    }
    
    @objc static var polygonColor: UIColor {
        return #colorLiteral(red: 0.231372549, green: 0.6980392157, blue: 0.8156862745, alpha: 1)
    }
}
