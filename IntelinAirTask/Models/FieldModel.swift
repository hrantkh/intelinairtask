//
//  FieldModel.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import Foundation

struct FieldModel: Codable {
    var field: Field?
    var company: CompanyModel?
    var latestFlight: LatestFlightModel?
}

extension FieldModel {
    
    func fieldTileName() -> String {
        return (company?.name ?? "") + " " + (field?.name ?? "")
    }
    
}
