//
//  FieldsDataModel.swift
//  IntelinAirTask
//
//  Created by Hrant on 4/20/19.
//  Copyright © 2019 Hrant. All rights reserved.
//

import Foundation

struct FieldsDataModel: Codable {
    var count: Int?
    var max: Int?
    var fields: [FieldModel]?
}
